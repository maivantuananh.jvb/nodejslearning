const User = require('../models/user');
class UserRepository {
    constructor(UserModel) {
        this.UserModel = UserModel;
    }

    async getUsers(){
        return await this.UserModel.findAll();
    }

    async findByEmail(email) {
        return await this.UserModel.findOne({ where: { email } });
    }

    async createUser(name, email, password, isBetaMember) {
        return await this.UserModel.create({ name, email, password, isBetaMember });
    }
}

module.exports = new UserRepository(User);