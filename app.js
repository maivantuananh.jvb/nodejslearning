//Use .env file
const dotenv = require('dotenv');
dotenv.config();


//Setup server
const bodyParser = require('body-parser');
const express = require('express');
const session = require('express-session');
const app = express();
app.set('views', './views');
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
//session sau bodyParser và trước đăng ký route
app.use(session({
    secret: 'ruguokeyi', resave: false, saveUninitialized: true, rolling: true, cookie: { maxAge: 3600000 },
}));
const helpers = require('./helpers/specialHelper');
const routeManager = require('./helpers/routerHelper');
app.use((req, res, next) => {
    res.locals.asset = helpers.asset;
    res.locals.route = routeManager.route;
    next();
});
app.use('/', require('./routes/web')(express));
app.use('/', require('./routes/api')(express));

const observers = require('./observers/index');
observers.initializeObservers();

app.listen(80);