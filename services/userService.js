const userRepository = require('../repositorys/userRepository');
const bcrypt = require('bcrypt');
class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async createUserIfNotExists(name, email, password) {
        const existingUser = await this.userRepository.findByEmail(email);
        if (existingUser) {
            return existingUser;
        }
        return await this.userRepository.createUser(name, email, await bcrypt.hash(password, 10), true);
    }

    async getUsers(res) {
        return await this.userRepository.getUsers();
        
    }

    async login(req, res) {
        const { email, password } = req.body;
        const user = await this.userRepository.findByEmail(email);
        if (!user || !bcrypt.compareSync(password, user.password)) {
            return res.render('login', { error: 'Email hoặc mật khẩu không đúng' });
        }
        req.session.user = user;
        const returnTo = req.session.returnTo;
        if (returnTo) {
            delete req.session.returnTo;
            return res.redirect(returnTo);
        } else {
            return res.redirect('/default');
        }
    }
}

module.exports = new UserService(userRepository);