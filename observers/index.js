const initializeObservers = function () {
    const observers = [
        'UserObserver',
    ];

    observers.forEach((observer) => {
        require(`./${observer}`);
    });
}

module.exports = {
    initializeObservers
};