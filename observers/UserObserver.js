const User = require('../models/User');

User.addHook('afterCreate', (user) => {
    console.log(`User ${user.name} is about to be created.`);
});

User.addHook('beforeUpdate', (user) => {
    console.log(`User ${user.name} is about to be updated.`);
});