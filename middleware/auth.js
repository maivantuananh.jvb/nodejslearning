class Auth {
    requireLogin(req, res, next) {
        if (!req.session.user) {
            req.session.returnTo = req.originalUrl;
            return res.redirect('/login');
        }
        return next();
    };

    encrypt(req, res, next) {
        if (!req.headers['secretkey']) {
            return res.status(403).send('Need to require SecretKey!');
        } else if (req.headers['secretkey'] !== process.env.SECRET_KEY) {
            return res.status(403).send('SecretKey it not right!');
        } else if (!req.session.user) {
            return res.status(403).send('Need to login!');
        }
        return next();
    };
}

module.exports = new Auth();