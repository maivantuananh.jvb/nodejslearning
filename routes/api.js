module.exports = (express) => {
    const router = express.Router();
    const IndexController = require('../controllers/indexController');
    const Auth = require('../middleware/auth');
    
    router.put('/getUsersJson', IndexController.getUsersJson);
    router.post('/createUser', IndexController.newUser);
    router.get('/request', IndexController.requestSession);

    return router;
};