module.exports = (express) => {
    const router = express.Router();
    const IndexController = require('../controllers/indexController');
    const Auth = require('../middleware/auth');
    const { register, setPrefix, unsetPrefix } = require('../helpers/routerHelper');

    router.get('/', Auth.requireLogin, IndexController.renderIndex);
    router.get('/default', IndexController.defaultX);
    router.get('/getUsers', IndexController.getUsers);
    router.get('/login', IndexController.renderLogin);
    router.post('/login', IndexController.login);

    //start
    const publicRouter = express.Router();
    const prefix = '/public';
    setPrefix(prefix);
    publicRouter.get(register('/test/:id/and/:name', 'testRoute'), IndexController.getParamsFromURL);
    publicRouter.get(register('/countdown', 'mycountdown'), IndexController.countdown);
    unsetPrefix();
    router.use(prefix, publicRouter);
    
    //end

    router.get(register('/login2', 'login2'), IndexController.defaultX);

    return router;
};