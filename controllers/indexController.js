const axios = require('axios');
const UserService = require('../services/userService');
const Log = require('../config/logger');

class IndexController {
    constructor() { }

    renderIndex(req, res) {
        const apiUrl = 'http://127.0.0.1:8000/api/v1/login';
        const token = 'your-bearer-token';
        const headers = {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        };
        const body = {
            "email": "tran.huyentrang@jvb-corp.com",
            "password": "12345678"
        };
        const pageTitle = 'Logined';
        axios.post(apiUrl, body, { headers })
            .then(response => {
                let responseData = response.data;
                return res.render('index', { pageTitle, responseData });
            })
            .catch(error => {
                return console.error('Lỗi khi gửi yêu cầu:', error);
            });
    };

    defaultX(req, res) {
        try {
            return res.send('《|defaultX page|》');
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }        
    };

    async getUsers(req, res) {
        try {
            const users = await UserService.getUsers(res);
            return res.render('users', { users });
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }
    };

    async countdown(req, res) {
        try {
            const pageTitle = 'Đồng hồ đếm ngược';
            const endTime = '2025-04-25T17:15:00'

            return res.render('my-countdown', { pageTitle, endTime });
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }
    };

    async renderLogin(req, res) {
        try {
            return res.render('login');
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }
    };

    async login(req, res) {
        try {
            return await UserService.login(req, res);
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }        
    };

    async getParamsFromURL(req, res) {
        try {
            return res.json(req.params);
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }      
        
    };

    //API_________________________________________________
    async requestSession(req, res) {
        return res.json(req.session);
    };

    async getUsersJson(req, res) {
        try {
            const users = await UserService.getUsers(res);
            return res.status(200).json(users);
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }
    };

    async newUser(req, res) {
        try {
            const newUser = await UserService.createUserIfNotExists('example', 'example@example.com', '12345678');
            return res.json(newUser);
        } catch (error) {
            Log.debugLog(error);
            return res.status(500).send("ERROR");
        }
    }
}

module.exports = new IndexController();