const { Sequelize, DataTypes } = require('sequelize');
const config = require('./config.json')[process.env.NODE_ENV];
const sequelize = new Sequelize(config);
module.exports = { sequelize, DataTypes };