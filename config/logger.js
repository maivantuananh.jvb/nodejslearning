const winston = require('winston');

class Logger {
    errorLog(error) {
        return winston.createLogger({
            level: 'error',
            format: winston.format.simple(),
            transports: [new winston.transports.File({ filename: 'log/error.log' })]
        }).error(error.stack);
    }

    debugLog(debug) {
        return winston.createLogger({
            level: 'debug',
            format: winston.format.simple(),
            transports: [new winston.transports.File({ filename: 'log/debug.log' })]
        }).error(debug.stack);
    }
}

module.exports = new Logger();