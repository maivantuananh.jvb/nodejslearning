const namedRoutes = {};

function addRoute(routeName, path) {
    namedRoutes[routeName] = path;
}

let routePrefix = '';

function setPrefix(prefix) {
    routePrefix = prefix;
}

function unsetPrefix() {
    routePrefix = null;
}

function generatePath(routeName, params = {}, withDomain = false) {
    const routePath = namedRoutes[routeName];
    if (!routePath) {
        throw new Error(`Route with name '${routeName}' does not exist.`);
    }
    let generatedPath = routePath;
    for (const paramName in params) {
        generatedPath = generatedPath.replace(`:${paramName}`, params[paramName]);
    }
    if (withDomain) {
        const domain = process.env.NODE_URL;
        return domain + generatedPath;
    }
    return generatedPath;
}

function getPath(routeName, params = {}) {
    return generatePath(routeName, params);
}

function register(path, routeName) {
    addRoute(routeName, path);
    return getPath(routeName);
}

function route(routeName, params = {}) {
    return generatePath(routeName, params, true) + routePrefix;
}

module.exports = {
    register,
    route,
    setPrefix,
    unsetPrefix
};
