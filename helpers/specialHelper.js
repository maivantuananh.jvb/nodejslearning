class SpecialHelper {
    asset(path) {
        const domain = process.env.NODE_URL;
        return domain + path;
    }

    route(path, params) {
        const domain = process.env.NODE_URL;
        return Object.keys(params).reduce((generatedRoute, param) => {
            return generatedRoute.replace(`:${param}`, params[param]);
        }, domain + path);
    }
}

module.exports = new SpecialHelper();