'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('users', [{
      name: 'Administrator',
      email: 'admin@nodejs.com',
      password: '$2b$10$jYVcdHrJJTUjgrIhSyz9aennHKDteuRfxfpE9ic8LsCPSxsd/Aa/u',
      isBetaMember: true
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});
  }
};
